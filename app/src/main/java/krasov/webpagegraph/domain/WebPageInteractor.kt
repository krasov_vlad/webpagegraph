package krasov.webpagegraph.domain

import android.util.Log
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import krasov.webpagegraph.mvp.model.Node
import krasov.webpagegraph.mvp.model.NodeViewed
import krasov.webpagegraph.utils.CommonUtils
import krasov.webpagegraph.utils.Constants
import org.jsoup.Jsoup
import org.jsoup.UnsupportedMimeTypeException
import java.io.IOException
import java.util.*
import java.util.concurrent.Executors
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.HashSet


/**
 * Created by user on 06.03.2018.
 */

class WebPageInteractor(val utils: CommonUtils) {


    private var flowable: Flowable<Node>
    private var emitter: FlowableEmitter<Node>? = null
    private var threadCount: Int = 1
    private var executor = Executors.newFixedThreadPool(threadCount)

    private var safeVisitedList = Collections.synchronizedList(ArrayList<Node>())

    init {
        flowable = Flowable.create({
            emitter = it
        }, BackpressureStrategy.BUFFER)
    }


    fun clear() {
        safeVisitedList.clear()
    }

    fun addRoot(node: Node) {
        safeVisitedList.add(node)
        emitter?.onNext(node)
    }

    fun start(find: String, maxCount: Int): Flowable<NodeViewed> {
        return flowable
                .parallel(threadCount)
                .runOn(Schedulers.from(executor))
                .map {
                    val viewed = viewNode(it, find)
                    viewed.childNodes?.forEach { child ->
                        if (maxCount > safeVisitedList.size) {
                            if (!safeVisitedList.contains(child)) {
                                safeVisitedList.add(it)
                                emitter?.onNext(child)
                            }
                        } else {
                            emitter?.onComplete()
                        }

                    }
                    return@map viewed
                }
                .doOnError { Log.e(TAG, it.message) }
                .sequential()
                .observeOn(AndroidSchedulers.mainThread())
    }


    @Throws(IOException::class, UnsupportedMimeTypeException::class)
    private fun viewNode(node: Node, find: String): NodeViewed {
        val doc = Jsoup.connect(node.href).get()
        val links = doc.select("a[href]")
        val childs = HashSet<Node>()
        links.forEach {
            if (utils.isHref(it.attr("href")))
                childs.add(Node(it.attr("href")))
        }
        val p = Pattern.compile(find)
        val matcher = p.matcher(doc?.body()?.text() ?: "")//if empty body
        var count = 0
        while (matcher.find()) {
            count++
        }
        return NodeViewed(node.href, count, childs)
    }

    fun setThredCount(count: Int) {
        threadCount = count
        executor = Executors.newFixedThreadPool(threadCount)
    }

    companion object {
        val TAG = Constants.TAG_PREFIX + "Interactor"
    }

    fun executorShutdown() {
        executor.shutdown()
    }
}
