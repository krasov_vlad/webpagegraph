package krasov.webpagegraph.mvp.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by user on 07.11.2017.
 */

interface BaseView : MvpView {

    @StateStrategyType(SkipStrategy::class)
    fun showToast(text: String)

}
