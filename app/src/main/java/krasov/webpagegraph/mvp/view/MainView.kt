package krasov.webpagegraph.mvp.view

import krasov.webpagegraph.mvp.model.NodeViewed

/**
 * Created by user on 07.11.2017.
 */

interface MainView : BaseView {

    fun initInputs()

    fun initButtons()

    fun updateInfo(textFound: Int, urlsVisited: Int)

    fun onProcessStart()

    fun onProcessStop()

    fun disableStart()

    fun enableStart()

    fun initLogRecyclerView()

    fun addLogItem(item: NodeViewed)

    fun clearLog()
}
