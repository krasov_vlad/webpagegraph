package krasov.webpagegraph.mvp.model

/**
 * Created by user on 06.03.2018.
 */

data class NodeViewed(val href: String, val textFindCount: Int, val childNodes: HashSet<Node>?)