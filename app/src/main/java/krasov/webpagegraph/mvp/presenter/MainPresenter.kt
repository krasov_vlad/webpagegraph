package krasov.webpagegraph.mvp.presenter

import android.text.Editable
import android.util.Log

import com.arellomobile.mvp.InjectViewState
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.FlowableOnSubscribe
import kotlinx.coroutines.experimental.launch

import javax.inject.Inject

import krasov.webpagegraph.App
import krasov.webpagegraph.domain.WebPageInteractor
import krasov.webpagegraph.mvp.model.Node
import krasov.webpagegraph.mvp.model.NodeViewed
import krasov.webpagegraph.mvp.view.MainView
import krasov.webpagegraph.utils.CommonUtils
import krasov.webpagegraph.utils.Constants
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import kotlin.collections.HashSet

/**
 * Created by user on 07.11.2017.
 */

@InjectViewState
class MainPresenter : BasePresenter<MainView>() {
    @Inject
    lateinit var interactor: WebPageInteractor
    @Inject
    lateinit var urils: CommonUtils

    private var maxVisitedCount = 10
    private var totalTextFound = 0
    private var totalUrlVisited = 0


    init {
        App.appComponent?.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.initButtons()
        viewState.initInputs()
        viewState.initLogRecyclerView()

    }


    private fun configNewSearch(text: String) {
        stopAndClear()
        addSubscription(interactor.start(text, maxVisitedCount)
                .subscribe(
                        {
                            totalTextFound += it.textFindCount
                            totalUrlVisited++
                            viewState.addLogItem(it)
                            viewState.updateInfo(totalTextFound, totalUrlVisited)
                            Log.d(TAG, it.href + " " + it.textFindCount)
                        },
                        {
                            Log.e(TAG, it.message)
                            viewState.onProcessStop()
                            viewState.showToast("Error")
                        },
                        {
                            viewState.onProcessStop()
                            viewState.showToast("Complete")
                        })
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        interactor.executorShutdown()
    }

    fun setThreadCount(s: CharSequence?) {
        try {
            interactor.setThredCount(Integer.parseInt(s.toString()))
            viewState.enableStart()
        } catch (e: NumberFormatException) {
            viewState.disableStart()
        }
    }

    fun setMaxVisitedUrlsCount(s: CharSequence?) {
        try {
            maxVisitedCount = Integer.parseInt(s.toString())
            viewState.enableStart()
        } catch (e: NumberFormatException) {
            viewState.disableStart()
        }
    }

    fun onStartClick(url: Editable, text: Editable) {
        if (urils.isHref(url.toString())) {
            if (text.isNotEmpty()) {
                configNewSearch(text.toString())
                interactor.addRoot(Node(url.toString()))
                viewState.onProcessStart()
            } else {
                viewState.showToast("Empty search str")
            }
        } else {
            viewState.showToast("URL INCORRECT")
        }
    }

    fun onStopClick() {
        stopAndClear()
    }

    private fun stopAndClear() {
        clearSubscriptions()
        totalTextFound = 0
        totalUrlVisited = 0
        viewState.onProcessStop()
        viewState.clearLog()
        interactor.clear()
    }

    companion object {
        private val TAG = Constants.TAG_PREFIX + "MainPresenter"
    }

}
