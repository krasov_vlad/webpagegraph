package krasov.webpagegraph.mvp.presenter

import com.arellomobile.mvp.MvpPresenter

import io.reactivex.annotations.NonNull
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import krasov.webpagegraph.mvp.view.BaseView

/**
 * Created by user on 07.11.2017.
 */

open class BasePresenter<View : BaseView> : MvpPresenter<View>() {
    private val compositeDisposable = CompositeDisposable()

    protected fun addSubscription(@NonNull disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    protected fun clearSubscriptions(){
        compositeDisposable.clear()
    }
}
