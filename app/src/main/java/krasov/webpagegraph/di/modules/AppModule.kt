package krasov.webpagegraph.di.modules

import android.content.Context

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import io.reactivex.annotations.NonNull
import krasov.webpagegraph.domain.WebPageInteractor
import krasov.webpagegraph.utils.CommonUtils

/**
 * Created by user on 07.11.2017.
 */

@Module
class AppModule(private val context: Context) {

    @Provides
    @NonNull
    @Singleton
    internal fun provideContext(): Context {
        return context
    }

    @Provides
    @Singleton
    internal fun provideCommonUtils(context: Context): CommonUtils {
        return CommonUtils(context)
    }

    @Provides
    @Singleton
    internal fun provideInteractor(commonUtils: CommonUtils): WebPageInteractor {
        return WebPageInteractor(commonUtils)
    }
}
