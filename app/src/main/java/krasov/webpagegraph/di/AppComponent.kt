package krasov.webpagegraph.di

import javax.inject.Singleton

import dagger.Component
import krasov.webpagegraph.di.modules.AppModule
import krasov.webpagegraph.mvp.presenter.MainPresenter

/**
 * Created by user on 07.11.2017.
 */


@Component(modules = arrayOf(AppModule::class))
@Singleton
interface AppComponent {
    fun inject(mainPresenter: MainPresenter)
}
