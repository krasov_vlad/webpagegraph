package krasov.webpagegraph.ui.activity

import android.annotation.SuppressLint
import android.widget.Toast

import com.arellomobile.mvp.MvpAppCompatActivity

import krasov.webpagegraph.mvp.view.BaseView

/**
 * Created by user on 07.11.2017.
 */

@SuppressLint("Registered")
open class BaseActivity : MvpAppCompatActivity(), BaseView {

    override fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }


}
