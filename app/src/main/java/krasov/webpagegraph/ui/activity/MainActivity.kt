package krasov.webpagegraph.ui.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View.VISIBLE
import android.widget.LinearLayout
import com.arellomobile.mvp.presenter.InjectPresenter
import krasov.webpagegraph.R

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import krasov.webpagegraph.mvp.model.NodeViewed
import krasov.webpagegraph.mvp.presenter.MainPresenter
import krasov.webpagegraph.mvp.view.MainView
import krasov.webpagegraph.ui.adapters.LogAdapter
import kotlin.math.log

class MainActivity : BaseActivity(), MainView {
    @InjectPresenter
    lateinit var presenter: MainPresenter
    var logAdapter: LogAdapter = LogAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
    }

    override fun initInputs() {
        presenter.setThreadCount(thread_count_et.text)
        thread_count_et.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                presenter.setThreadCount(s)
            }
        })

        presenter.setMaxVisitedUrlsCount(max_urls_count_et.text)
        max_urls_count_et.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                presenter.setMaxVisitedUrlsCount(s)
            }
        })
    }

    override fun initButtons() {
        start_process_b.setOnClickListener { presenter.onStartClick(url_et.text, text_search_et.text) }
        stop_process_b.setOnClickListener { presenter.onStopClick() }
    }

    override fun updateInfo(textFound: Int, urlsVisited: Int) {
        text_found_count_et.setText(textFound.toString())
        urls_passed_et.setText(urlsVisited.toString())
    }

    override fun onProcessStart() {
        stop_process_b.isEnabled = true
        start_process_b.isEnabled = false


        url_et.isEnabled = false
        text_search_et.isEnabled = false
        max_urls_count_et.isEnabled = false
        thread_count_et.isEnabled = false
    }

    override fun disableStart() {
        start_process_b.isEnabled = false
    }

    override fun enableStart() {
        start_process_b.isEnabled = true
    }

    override fun onProcessStop() {
        stop_process_b.isEnabled = false
        start_process_b.isEnabled = true

        max_urls_count_et.isEnabled = true
        thread_count_et.isEnabled = true
        url_et.isEnabled = true
        text_search_et.isEnabled = true
    }

    override fun initLogRecyclerView() {
        log_recycler_view.layoutManager = LinearLayoutManager(this)
        log_recycler_view.adapter = logAdapter
    }

    override fun addLogItem(item: NodeViewed) {
        logAdapter.addItem(item)
    }

    override fun clearLog() {
        logAdapter.clearItems()
    }
}
