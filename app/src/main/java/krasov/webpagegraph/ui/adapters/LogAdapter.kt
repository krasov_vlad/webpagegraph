package krasov.webpagegraph.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import java.util.ArrayList

import krasov.webpagegraph.R
import krasov.webpagegraph.mvp.model.NodeViewed

/**
 * Created by user on 07.03.2018.
 */

class LogAdapter : RecyclerView.Adapter<LogAdapter.VHolder>() {
    private val items = ArrayList<NodeViewed>()

    fun addItem(item: NodeViewed) {
        items.add(item)
        notifyDataSetChanged()
    }

    fun clearItems() {
        items.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.log_item, parent, false)
        return VHolder(v)
    }

    override fun onBindViewHolder(holder: VHolder, position: Int) {
        holder.url.text = items[position].href
        holder.count.text = "text found: " + items[position].textFindCount.toString()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class VHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var url: TextView
        internal var count: TextView

        init {
            url = itemView.findViewById(R.id.log_item_url)
            count = itemView.findViewById(R.id.log_item_found_count)
        }
    }
}
