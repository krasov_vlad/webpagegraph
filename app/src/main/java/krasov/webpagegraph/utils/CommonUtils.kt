package krasov.webpagegraph.utils

import android.content.Context
import android.net.ConnectivityManager
import android.util.Patterns
import android.webkit.URLUtil

/**
 * Created by user on 15.08.2017.
 */

class CommonUtils(private val context: Context) {

    val isNetworkConnected: Boolean
        get() {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    fun isHref(url: String): Boolean {
        return Patterns.WEB_URL.matcher(url).matches()
    }

    companion object {
        private val TAG = Constants.TAG_PREFIX + "CommonUtils"
    }


}
