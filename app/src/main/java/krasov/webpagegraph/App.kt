package krasov.webpagegraph

import android.app.Application


import krasov.webpagegraph.di.AppComponent
import krasov.webpagegraph.di.DaggerAppComponent
import krasov.webpagegraph.di.modules.AppModule

/**
 * Created by user on 07.11.2017.
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = buildAppComponent()
    }

    private fun buildAppComponent(): AppComponent {
        return DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    companion object {
        var appComponent: AppComponent? = null
            private set
    }
}
